import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrimeGeneratorComponent } from './prime-generator/prime-generator.component';

const routes: Routes = [
  { 
    path: '',
    component: PrimeGeneratorComponent 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
