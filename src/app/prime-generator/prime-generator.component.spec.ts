import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeGeneratorComponent } from './prime-generator.component';

describe('PrimeGeneratorComponent', () => {
  let component: PrimeGeneratorComponent;
  let fixture: ComponentFixture<PrimeGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
