import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrimeService {

  private apiServerUrl = environment.apiBaseUrl;
  
  constructor(private http: HttpClient) { }

  public getPrimeNumbers(inputNumber: number): Observable<Array<number>>{
    return this.http.get<Array<number>>(`${this.apiServerUrl}/v1/prime/${inputNumber}`);
  }
  
}
