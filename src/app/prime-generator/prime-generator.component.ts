import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PrimeService } from './prime.service';

@Component({
  selector: 'app-prime-generator',
  templateUrl: 'prime-generator.component.html',
  styleUrls: ['./prime-generator.component.css']
})
export class PrimeGeneratorComponent implements OnInit {

  constructor(private primeService: PrimeService) { }
  inputForm!: FormGroup;
  primeNumbers: number[] = [];
  isGreaterThanOne!: boolean;

  ngOnInit(): void {
    this.inputForm = new FormGroup({
      'inputNumber' : new FormControl(null, [Validators.required,Validators.pattern("[1-9 ]{1,10}")]),
    });
  }

  calculatePrimes(){
    
    const inputNumber = this.inputForm.value['inputNumber'];
    if(inputNumber == 1){
      this.isGreaterThanOne = false;
    }else{
      this.isGreaterThanOne = true;
    }

    this.primeService.getPrimeNumbers(inputNumber).subscribe(ans =>{
      this.primeNumbers = ans;
    });
  }

}
